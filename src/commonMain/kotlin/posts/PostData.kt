/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib.posts

import io.ktor.http.Url
import net.wildfyre.lib.users.User
import net.wildfyre.lib.utils.CachedObject

/**
 * An interface to specify attributes common to [drafs][Draft] and [posts][Post].
 */
interface PostData : CachedObject<Int, PostData> {

    /**
     * The author of this object, if known.
     */
    suspend fun author(): User?

    /**
     * Is this post anonymous?
     */
    val anonymous: Boolean

    /**
     * The text contained by this object.
     *
     * The text can be written in MarkDown.
     */
    val text: String

    /**
     * The main image of this object, if any.
     */
    val mainImage: Url?

    /**
     * Any secondary images contained by this object.
     */
    val secondaryImages: List<Url>

}

